//
//  Logger+Ext.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/12.
//

import os.log
import Foundation

extension Logger {
    private static var subsystem: String { Bundle.main.bundleIdentifier! }
    
    static let scan = Logger(subsystem: subsystem, category: "scan")
    static let pair = Logger(subsystem: subsystem, category: "pair")
    static let connection = Logger(subsystem: subsystem, category: "connection")
    static let streaming = Logger(subsystem: subsystem, category: "streaming")
    static let sync = Logger(subsystem: subsystem, category: "sync")
    static let fetch = Logger(subsystem: subsystem, category: "fetch")
    static let autoSync = Logger(subsystem: subsystem, category: "autoSync")
    static let autoFetch = Logger(subsystem: subsystem, category: "autoFetch")
}
