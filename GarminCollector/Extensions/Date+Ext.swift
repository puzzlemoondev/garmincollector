//
//  Date+Ext.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/13.
//

import Foundation

extension Date {
    func nearestHourStart() -> Date {
        Date(timeIntervalSinceReferenceDate: (timeIntervalSinceReferenceDate / 3600.0).rounded(.down) * 3600.0)
    }
    
    func nextHourStart() -> Date {
        nearestHourStart().addingTimeInterval(3600)
    }
}

extension Date: Strideable {
    public func distance(to other: Date) -> TimeInterval {
        return other.timeIntervalSinceReferenceDate - self.timeIntervalSinceReferenceDate
    }

    public func advanced(by n: TimeInterval) -> Date {
        return self + n
    }
}
