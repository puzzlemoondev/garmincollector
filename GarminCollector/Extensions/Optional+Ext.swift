//
//  Optional+Ext.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/13.
//

import Foundation

extension Optional where Wrapped: Comparable {
    public static func > (lhs: Self, rhs: Self) -> Bool {
        if let lhs = lhs, let rhs = rhs {
            return lhs > rhs
        } else {
            return lhs != nil
        }
    }
}
