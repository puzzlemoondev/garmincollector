//
//  GarminCollectorApp.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/2/25.
//

import SwiftUI

@main
struct GarminCollectorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: ContentViewModel())
        }
    }
}
