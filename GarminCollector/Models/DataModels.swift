//
//  BBIModels.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/4/13.
//

import Foundation
import companion

struct HourlyBBIs {
    let startHour: Date
    var BBIs: [GHBBIData]
    var stats: BBIStats {
        let totalMins = BBIs.compactMap { $0.milliseconds?.doubleValue }.reduce(0.0, +) / 60000
        let bbiCounts = BBIs.count
        let missingMins = 60 - totalMins
        return BBIStats(startTime: startHour,
                        endTime: startHour.nextHourStart(),
                        total: totalMins,
                        missing: missingMins < 0 ? 0 : missingMins,
                        samples: bbiCounts)
    }
}

extension HourlyBBIs: Comparable {
    static func < (lhs: Self, rhs: Self) -> Bool {
        lhs.startHour < rhs.startHour
    }
}

extension Array where Element == HourlyBBIs {
    mutating func merge(_ other: Self) {
        for element in other {
            if let index = firstIndex(where: { $0.startHour == element.startHour }) {
                let newBBIs = element.BBIs.filter { bbi in !self[index].BBIs.contains { $0.timestamp == bbi.timestamp } }
                self[index].BBIs.append(contentsOf: newBBIs)
                self[index].BBIs.sort()
            } else {
                self.append(element)
            }
        }
        self.sort()
    }
}

struct BBIStats {
    let startTime: Date
    let endTime: Date
    let total: TimeInterval
    let missing: TimeInterval
    let samples: Int
}

extension BBIStats {
    var statsString: String {
        "from \(startTime.description) to \(endTime.description), total: \(total.fractionDigitsRounded(to: 2))mins, missing: \(missing.fractionDigitsRounded(to: 2))mins, samples: \(samples)"
    }
}

extension BBIStats {
    static func + (lhs: Self, rhs: Self) -> Self {
        return BBIStats(startTime: lhs.startTime < rhs.startTime ? lhs.startTime : rhs.startTime,
                        endTime: lhs.endTime > rhs.endTime ? lhs.endTime : rhs.endTime,
                        total: lhs.total + rhs.total,
                        missing: lhs.missing + rhs.missing,
                        samples: lhs.samples + rhs.samples)
    }
}
