//
//  GarminManager.swift
//  GarminCollector
//
//  Created by Kiipo-D on 2021/2/25.
//

import companion
import RxSwift
import RxRelay

enum GarminManagerError: Error, CustomStringConvertible, LocalizedError {
    case notInitialized
    case setLoggingStateFailed
    case noDataFetched
    case syncRequestDenied
    
    var description: String {
        switch self {
        case .notInitialized:
            return "GHDeviceManaget is not initialized"
        case .setLoggingStateFailed:
            return "set logging state failed"
        case .noDataFetched:
            return "no data fetched"
        case .syncRequestDenied:
            return "sync request denied by device"
        }
    }
    
    var errorDescription: String? {
        NSLocalizedString(description, comment: "error description")
    }
}

final class GarminManager: NSObject {
    static let shared = GarminManager()

    static let deviceTypes: [NSNumber] = [
        NSNumber(value: GHDeviceTypes.venu.rawValue),
        NSNumber(value: GHDeviceTypes.venuSq.rawValue),
        NSNumber(value: GHDeviceTypes.vivoactive3.rawValue),
        NSNumber(value: GHDeviceTypes.vivoactive3m.rawValue),
        NSNumber(value: GHDeviceTypes.vivoactive4.rawValue),
        NSNumber(value: GHDeviceTypes.vivoactive4Legacy.rawValue),
        NSNumber(value: GHDeviceTypes.vivoactive4s.rawValue),
        NSNumber(value: GHDeviceTypes.vivoactiveHr.rawValue),
        NSNumber(value: GHDeviceTypes.vivosmart3.rawValue),
        NSNumber(value: GHDeviceTypes.vivosmart4.rawValue),
        NSNumber(value: GHDeviceTypes.vivosmartHr.rawValue),
        NSNumber(value: GHDeviceTypes.vivomove3.rawValue),
        NSNumber(value: GHDeviceTypes.vivomove3s.rawValue),
        NSNumber(value: GHDeviceTypes.vivomoveHr.rawValue),
        NSNumber(value: GHDeviceTypes.vivomoveLuxe.rawValue),
        NSNumber(value: GHDeviceTypes.vivomoveStyle.rawValue),
        NSNumber(value: GHDeviceTypes.vivosport.rawValue),
        NSNumber(value: GHDeviceTypes.fenix5.rawValue),
        NSNumber(value: GHDeviceTypes.fenix5s.rawValue),
        NSNumber(value: GHDeviceTypes.fenix5x.rawValue),
        NSNumber(value: GHDeviceTypes.fenix5Plus.rawValue),
        NSNumber(value: GHDeviceTypes.fenix5sPlus.rawValue),
        NSNumber(value: GHDeviceTypes.fenix5xPlus.rawValue),
        NSNumber(value: GHDeviceTypes.fenix6.rawValue),
        NSNumber(value: GHDeviceTypes.fenix6Pro.rawValue),
        NSNumber(value: GHDeviceTypes.fenix6ProSolar.rawValue),
        NSNumber(value: GHDeviceTypes.fenix6s.rawValue),
        NSNumber(value: GHDeviceTypes.fenix6sPro.rawValue),
        NSNumber(value: GHDeviceTypes.fenix6xPro.rawValue),
        NSNumber(value: GHDeviceTypes.forerunner245.rawValue),
        NSNumber(value: GHDeviceTypes.forerunner245m.rawValue),
        NSNumber(value: GHDeviceTypes.forerunner645.rawValue),
        NSNumber(value: GHDeviceTypes.forerunner645m.rawValue),
        NSNumber(value: GHDeviceTypes.forerunner945.rawValue),
        NSNumber(value: GHDeviceTypes.instinct.rawValue),
        NSNumber(value: GHDeviceTypes.lily.rawValue),
        NSNumber(value: GHDeviceTypes.tactixCharlie.rawValue),
        NSNumber(value: GHDeviceTypes.tactixDelta.rawValue),
        NSNumber(value: GHDeviceTypes.marqDriver.rawValue),
        NSNumber(value: GHDeviceTypes.marqAviator.rawValue),
        NSNumber(value: GHDeviceTypes.marqCaptain.rawValue),
        NSNumber(value: GHDeviceTypes.marqExpedition.rawValue),
        NSNumber(value: GHDeviceTypes.marqAthlete.rawValue),
        NSNumber(value: GHDeviceTypes.marqCommander.rawValue)
    ]
    
    // MARK: Scanning
    func scan() {
        GHDeviceManager.shared()?.scan(forDevices: Self.deviceTypes)
    }
    
    func stopScan() {
        GHDeviceManager.shared()?.stopScan()
    }

    // MARK: Pairing
    func pair(with device: GHScannedDevice) {
        GHDeviceManager.shared()?.pairDevice(device, delegate: self as GHPairingDelegate)
    }
    
    func unpair(with device: GHDevice) {
        GHDeviceManager.shared()?.forget(device)
    }
    
    func getPairedDevices() -> [GHDevice] {
        GHDeviceManager.shared()?.getPairedDevices() ?? []
    }
    
    // MARK: Device
    func getDevice(withId deviceId: UUID) -> GHDevice? {
        GHDeviceManager.shared()?.getDevice(deviceId)
    }
    
    func getRealTimeDevice() -> Observable<GHRealTimeDevice> {
        deviceRelay.asObservable()
    }
    
    // MARK: Streaming
    func startStreaming(for device: GHRealTimeDevice, dataTypes: GHRealTimeTypes) {
        device.add(self as GHRealTimeDelegate)
        device.startListening(for: dataTypes)
    }
    
    func stopStreaming(for device: GHRealTimeDevice, dataTypes: GHRealTimeTypes) {
        device.stopListening(to: dataTypes)
        device.remove(self as GHRealTimeDelegate)
    }
    
    // MARK: Logging
    func startLogging(for device: GHRealTimeDevice, dataTypes: GHLoggingDataTypes) -> Completable {
        setLogging(to: true, for: device, dataTypes: dataTypes)
    }
    
    func stopLogging(for device: GHRealTimeDevice, dataTypes: GHLoggingDataTypes) -> Completable {
        setLogging(to: false, for: device, dataTypes: dataTypes)
    }
    
    func getLoggingState(for device: GHRealTimeDevice, dataTypes: GHLoggingDataTypes) -> Single<GHLoggingState> {
        Single<GHLoggingState>.create { (single) -> Disposable in
            device.getLoggingState(forDataType: dataTypes) { (enabled, interval, error) in
                if let error = error {
                    single(.failure(error))
                } else {
                    if let interval = interval?.intValue {
                        single(.success((enabled, interval)))
                    } else {
                        single(.success((enabled, nil)))
                    }
                }
            }

            return Disposables.create()
        }
    }
    
    private func setLogging(to state: Bool, for device: GHRealTimeDevice, dataTypes: GHLoggingDataTypes) -> Completable {
        Completable.create { completable -> Disposable in
            device.setLoggingState(state, forDataType: dataTypes) { (enabled, _, error) in
                if let error = error {
                    completable(.error(error))
                } else if enabled != state {
                    completable(.error(GarminManagerError.setLoggingStateFailed))
                } else {
                    completable(.completed)
                }
            }
            
            return Disposables.create()
        }
    }
    
    // MARK: Sync
    func requestSync(for device: GHRealTimeDevice) -> Completable {
        Completable.create { completable -> Disposable in
            let result = device.requestSync()
            if result {
                completable(.completed)
            } else {
                completable(.error(GarminManagerError.syncRequestDenied))
            }
            return Disposables.create()
        }
        .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
    }

    // MARK: Fetch
    func fetchData(for device: GHRealTimeDevice, dataTypes: GHRequestDataTypes, from fromDate: Date, to toDate: Date) -> Single<GHFetchResult> {
        Single.create { single -> Disposable in
            guard let manager = GHDeviceManager.shared() else {
                single(.failure(GarminManagerError.notInitialized))
                return Disposables.create()
            }
            
            manager.getDataFor(device, dataTypes: dataTypes, from: fromDate, to: toDate) { fetchResult in
                if let fetchResult = fetchResult, fetchResult.contains(dataTypes: dataTypes) {
                    single(.success(fetchResult))
                } else {
                    single(.failure(GarminManagerError.noDataFetched))
                }
            }

            return Disposables.create()
        }
    }

    // MARK: Result Observers
    func getPairingResult(for deviceId: UUID? = nil) -> Observable<(UUID, Result<GHRealTimeDevice, Error>)> {
        pairingRelay
            .compactMap { result in
                guard let deviceId = deviceId else { return result }
                return deviceId == result.0 ? result : nil
            }
    }
    
    func getRealTimeResult(for deviceId: UUID? = nil) -> Observable<(GHRealTimeDevice, Result<GHRealTimeResult, Error>)> {
        realTimeRelay
            .compactMap { result in
                guard let deviceId = deviceId else { return result }
                return deviceId == result.0.identifier ? result : nil
            }
    }
    
    func getScanResult() -> Observable<Result<GHScannedDevice, Error>> {
        scanRelay.asObservable()
    }
    
    func getSyncResult(for deviceId: UUID? = nil) -> Observable<(UUID, GHSyncResult)> {
        syncRelay
            .compactMap { result in
                guard let deviceId = deviceId else { return result }
                return deviceId == result.0 ? result : nil
            }
    }

    // MARK: - Private
    private let disposeBag = DisposeBag()
    private let deviceRelay = PublishRelay<GHRealTimeDevice>()
    private let pairingRelay = PublishRelay<(UUID, Result<GHRealTimeDevice, Error>)>()
    private let realTimeRelay = PublishRelay<(GHRealTimeDevice, Result<GHRealTimeResult, Error>)>()
    private let scanRelay = PublishRelay<Result<GHScannedDevice, Error>>()
    private let syncRelay = PublishRelay<(UUID, GHSyncResult)>()

    private override init() {
        super.init()
        initializeGarminSDK()
    }
    
    private func initializeGarminSDK() {
        GHInitializer.sharedManager()?.initializeLicense(
            "EoACf8nXF1zS/miUksETjYjQl9oF6A9Hym2LI9FXHFiNZzI4XN4kVpy4woeOlKxQkoqs9RbAWfA5YSH/crs4DQB73ps2r25U3SBnxESd+dQLr31gjwtFnpyxxsO0p6bgJSKneZRptKiZmmlmya8UDruluDWZ865LJa4RPv7bKVkbMfWK0VBBunHM6KpYGkXUSeLTDD9P85XOiSXr7F75F/pRyI+mtWtoGtY8/Mor45vKSbH1cwK4tKZ+LwzvQubdYStO6y3m+P+v5IFU4i4nn4snWMyC5YdToGVkA/rEbvzbcK50ESRRf+I3vfehCIe5F8f4MR2Pp6Cn3CSykSXa5Br+8SIKAQIDBAUGBwgJCioIAQIDBAUHCAk="
        )

        GHDeviceManager.shared()?.add(self as GHSyncDelegate)
        GHDeviceManager.shared()?.add(self as GHDeviceConnectionDelegate)
        GHDeviceManager.shared()?.setScannerDelegate(self as GHScanDelegate)
    }
}

extension GarminManager: GHDeviceConnectionDelegate {
    func didConnect(_ device: GHRealTimeDevice?) {
        guard let device = device else { return }
        deviceRelay.accept(device)
    }

    func didDisconnectDevice(_ device: GHRealTimeDevice?) {
        guard let device = device else { return }
        deviceRelay.accept(device)
    }
}

extension GarminManager: GHPairingDelegate {
    func didPausePairing(_ completion: GHPairingCompletion?) {
        completion?.complete(self as GHPairingDelegate)
    }

    func didPairDevice(_ device: GHRealTimeDevice?) {
        guard let device = device else { return }
        pairingRelay.accept((device.identifier, .success(device)))
    }

    func didFail(toPairDevice deviceId: UUID?, error: Error?) {
        guard let deviceId = deviceId, let error = error else { return }
        pairingRelay.accept((deviceId, .failure(error)))
    }
}

extension GarminManager: GHRealTimeDelegate {
    func didReceive(_ result: GHRealTimeResult, ofType type: GHRealTimeTypes, from device: GHRealTimeDevice) {
        realTimeRelay.accept((device, .success(result)))
    }

    func didReceiveError(_ error: Error, from device: GHRealTimeDevice) {
        realTimeRelay.accept((device, .failure(error)))
    }
}

extension GarminManager: GHScanDelegate {
    func didScanDevice(_ device: GHScannedDevice?) {
        guard let device = device else { return }
        scanRelay.accept(.success(device))
    }
    
    func scanDidFailWithError(_ error: Error?) {
        guard let error = error else { return }
        scanRelay.accept(.failure(error))
    }
}

extension GarminManager: GHSyncDelegate {
    func didStartSync(withDevice deviceId: UUID?) {
        guard let deviceId = deviceId else { return }
        syncRelay.accept((deviceId, .started))
    }
    
    func didReceiveSyncProgress(_ progress: Double, deviceId: UUID?) {
        guard let deviceId = deviceId else { return }
        syncRelay.accept((deviceId, .inProgress(progress)))
    }
    
    func didStallSync(withDevice deviceId: UUID?, completion shouldStop: ((Bool) -> Void)?) {
        shouldStop?(true)
    }
    
    func didCompleteSync(withDevice deviceId: UUID?, error: Error?) {
        guard let deviceId = deviceId else { return }
        
        let nsError = error as NSError?
        if (nsError?.domain == GHSyncErrorDomain && nsError?.code == 0) || nsError == nil {
            syncRelay.accept((deviceId, .completed))
        } else {
            syncRelay.accept((deviceId, .failure(nsError!)))
        }
    }
}
